<?php

namespace Tests\App\Http\Controllers;

use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;
use App\Book;

class BooksControllerTest extends TestCase
{
	use DatabaseMigrations;

	public function testIndexStatusCodeShouldBe200()
	{
		$this->get('/books')->seeStatusCode(200);
	}

	public function testIndexShouldReturnACollectionOfRecords()
	{
		$books = factory(Book::class, 2)->create();
		$this->get('/books');

		foreach ($books as $book) {
			$this->seeJson(['title' => $book->title]);
		}
	}

	public function testShowShouldReturnAValidBook()
	{
		$book = factory(Book::class)->create();
		$this->get("/books/{$book->id}")
			->seeStatusCode(200)
			->seeJson([
				'id'          => $book->id,
				'title'       => $book->title,
				'description' => $book->description,
				'author'      => $book->author,
			]);

		$data = json_decode($this->response->getContent(), true);
		$this->assertArrayHasKey('created_at', $data);
		$this->assertArrayHasKey('updated_at', $data);
	}

	public function testShowShouldFailWhenTheBookIdDoesNotExist()
	{
		$this
			->get('/books/99999')
			->seeStatusCode(404)
			->seeJson([
				'error' => [
					'message' => 'Book not found',
				],
			]);
	}

	public function testShowRouteShouldNotMatchAnIndividualRoute()
	{
		$this->get('/books/this-is-invalid');
		$this->assertNotRegExp(
			'/Book not found/',
			$this->response->getContent(),
			'BooksController@show route matching when it should not.'
		);
	}

	public function testStoreShouldSaveNewBookInTheDatabase()
	{
		$this->post('/books', [
			'title'       => 'The Invisible Man',
			'description' => 'An invisible man is trapped in the terror of his own c\
reation',
			'author'      => 'H. G. Wells',
		]);

		$this
			->seeJson(['created' => true])
			->seeInDatabase('books', ['title' => 'The Invisible Man']);
	}

	public function testStoreShouldRespondWithA201AndLocationHeaderWhenSuccess()
	{
		$this->post('/books', [
			'title'       => 'The Invisible Man',
			'description' => 'An invisible man is trapped in the terror of his own c\
reation',
			'author'      => 'H. G. Wells',
		]);

		$this
			->seeStatusCode(201)
			->seeHeaderWithRegExp('Location', '#/books/[\d]+$#');
	}

	public function testUpdateShouldOnlyChangeFillableFields()
	{
		$book = factory(Book::class)->create([
			'title' => 'War of the Worlds',
			'description' => 'A science fiction masterpiece about Martians invading \
London',
			'author' => 'H. G. Wells',
		]);

		$this->put("/books/{$book->id}", [
			'id'          => 5,
			'title'       => 'The War of the Worlds',
			'description' => 'The book is way better than the movie.',
			'author'      => 'Wells, H. G.',
		]);

		$this
			->seeStatusCode(200)
			->seeJson([
				'id'          => 1,
				'title'       => 'The War of the Worlds',
				'description' => 'The book is way better than the movie.',
				'author'      => 'Wells, H. G.',
			])
			->seeInDatabase('books', [
				'title' => 'The War of the Worlds',
			]);
	}

	public function testUpdateShouldFailWithAnInvalidId()
	{
		$this
			->put('/books/99999999999')
			->seeStatusCode(404)
			->seeJsonEquals([
				'error' => [
					'message' => 'Book not found',
				],
			]);

	}

	public function testUpdateShouldNotMatchAnInvalidRoute()
	{
		$this
			->put('/books/this-is-invalid')
			->seeStatusCode(404);
	}

	public function testDestroyShouldRemoveAValidBook()
	{
		$book = factory(Book::class)->create();

		$this
			->delete("/books/{$book->id}")
			->seeStatusCode(204)
			->isEmpty();

		$this->notSeeInDatabase('books', ['id' => $book->id]);
	}

	public function testDestroyShouldReturn404WithAnInvalidId()
	{
		$this
			->delete('/books/9999999')
			->seeStatusCode(404)
			->seeJsonEquals([
				'error' => [
					'message' => 'Book not found',
				],
			]);
	}

	public function testDestroyShouldNotMatchAnInvalidRoute()
	{
		$this
			->delete('/books/this-is-invalid')
			->seeStatusCode(404);
	}
}