<?php
namespace Tests\App\Exceptions;


use TestCase;
use \Mockery as m;
use App\Exceptions\Handler;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;


class HandlerTest extends TestCase
{
	public function testItRespondsWithHtmlWhenJsonIsNotAccepted()
	{
		// Make the mock a partial, we only want to mock the `isDebugMode` method
		$subject = m::mock(Handler::class)->makePartial();
		$subject->shouldNotReceive('isDebugMode');

		// Mock the interaction with the Request
		$request = m::mock(Request::class);
		$request->shouldReceive('wantsJson')->andReturn(false);
		$request->shouldReceive('expectsJson')->andReturn(false);

		// Mock the interaction with the exception
		$exception = m::mock(\Exception::class, ['Error!']);
		$exception->shouldNotReceive('getStatusCode');
		$exception->shouldNotReceive('getTrace');
		$exception->shouldNotReceive('getMessage');

		// Call the method under test, this is not a mocked method.
		$result = $subject->render($request, $exception);

		// Assert that `render` does not return a JsonResponse
		$this->assertNotInstanceOf(JsonResponse::class, $result);
	}

	public function testItRespondsWithJsonForJsonConsumers()
	{
		$subject = m::mock(Handler::class)->makePartial();
		$subject
			->shouldReceive('isDebugMode')
			->andReturn(false);

		$request = m::mock(Request::class);
		$request
			->shouldReceive('wantsJson')
			->andReturn(true);

		$exception = m::mock(\Exception::class, ['Doh!']);
		$exception
			->shouldReceive('getMessage')
			->andReturn('Doh!');

		$result = $subject->render($request, $exception);

		$data = $result->getData();
		$this->assertInstanceOf(JsonResponse::class, $result);
		$this->assertObjectHasAttribute('error', $data);
		$this->assertAttributeEquals('Doh!', 'message', $data->error);
		$this->assertAttributeEquals(400, 'status', $data->error);

	}
}